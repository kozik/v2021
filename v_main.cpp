#include "v.h"
#include "verilated.h"

#include <stdio.h>

v *top = NULL;
vluint64_t main_time = 0;

double sc_time_stamp ()
// Called by $time in Verilog
{
    return main_time;
}


int main(int argc, char** argv, char** env) {
    top = new v;

    top->clk = 0;
    top->a = 5;
    top->b = 4;

    printf("time clk a b s\r\n");

    while (!Verilated::gotFinish()) {
        top->clk = main_time % 2;

        top->eval();

        printf("%5ld %d %3d %3d %3d\r\n", main_time,
                top->clk, top->a, top->b, top->s);

        if (main_time >= 10)
            break;

        main_time++;
    }

    delete top;
    exit(0);
}
