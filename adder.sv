`default_nettype none

module adder #(
    parameter WIDTH = 8
) (
    input wire clk,
    input wire [WIDTH-1:0]a,
    input wire [WIDTH-1:0]b,
    output logic [WIDTH:0]s
);

    logic [WIDTH-1:0]a_r;
    logic [WIDTH-1:0]b_r;

    always_ff @(posedge clk) begin
        a_r <= a;
        b_r <= b;
        s <= a_r + b_r;
    end

endmodule

`default_nettype wire

